import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "/home/pi/lavalrotor_home_experiment/datasheets/setup_dryer.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
uuid_sensor = sensor_settings_dict["ID"]
acceleration_x = []
acceleration_y = []
acceleration_z = []
timestamp = []
sensor_data = {uuid_sensor:{"acceleration_x":acceleration_x, "acceleration_y":acceleration_y,"acceleration_z":acceleration_z, "timestamp":timestamp}}

# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
measured_duration = 0
while measured_duration <= measure_duration_in_s:
    startzeit = time.time()
    acceleration = accelerometer.acceleration
    sensor_data[uuid_sensor]["acceleration_x"].append(acceleration[0])
    sensor_data[uuid_sensor]["acceleration_y"].append(acceleration[1])
    sensor_data[uuid_sensor]["acceleration_z"].append(acceleration[2])
    endzeit = time.time()
    sensor_data[uuid_sensor]["timestamp"].append(endzeit)
    measured_duration = measured_duration + (endzeit-startzeit)
    time.sleep(0.001)
# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
with h5py.File(path_h5_file, "a") as file:
    print(type(file))
    grp_raw = file.create_group("RawData")
    uuid_sensor_d = grp_raw.create_group(uuid_sensor)
    acceleration_x_d = uuid_sensor_d.create_dataset("acceleration_x", data = sensor_data[uuid_sensor]["acceleration_x"])
    acceleration_y_d = uuid_sensor_d.create_dataset("acceleration_y", data = sensor_data[uuid_sensor]["acceleration_y"])
    acceleration_z_d = uuid_sensor_d.create_dataset("acceleration_z", data = sensor_data[uuid_sensor]["acceleration_z"])
    timestamp_d = uuid_sensor_d.create_dataset("timestamp", data = sensor_data[uuid_sensor]["timestamp"])
    acceleration_x_d.attrs["unit"] = "m/s^2"
    acceleration_y_d.attrs["unit"] = "m/s^2"
    acceleration_z_d.attrs["unit"] = "m/s^2"
    timestamp_d.attrs["unit"] = "second"
    



# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
